#!/usr/bin/env bash

PPI_ROOT=$(readlink -f $HOME/.ppi/)

PPI_DESC=$PPI_ROOT/db/desc/

PPI_SRCPKG=$PPI_ROOT/pkg/src/
PPI_BINPKG=$PPI_ROOT/pkg/bin/
PPI_STOW=$PPI_ROOT/pkg/stow/

PPI_SRC_COMPRESS=gzip
PPI_BIN_COMPRESS=gzip
PPI_SRCEXT=.src.tar.gz
PPI_BINEXT=.pkg.tar.gz

PPI_ENV=${PPI_ENV:-default}

PPI_PREFIX=${PPI_ROOT}/env/${PPI_ENV}/

PPI_INSTALLED=$PPI_ROOT/db/installed

STOW_VERSION=2.2.0
STOW=$PPI_STOW/stow-${STOW_VERSION}/bin/stow
PPI_VERSION=master
PPI=$PPI_STOW/ppi-${PPI_VERSION}/bin/ppi

export PYTHONHOME=$PPI_PREFIX:$PPI_PREFIX

#export TMPDIR=/tmp/ppi-$(id -u)/
#mkdir -p $TMPDIR

export PATH="$(readlink -f $(dirname $0)):$PATH"

mkdir -p $PPI_SRC $PPI_PKG $PPI_DESC \
    $PPI_STOW $PPI_ROOT/db/ $PPI_ROOT/env/$PPI_ENV
mkdir -p $PPI_PREFIX/{bin,etc,games,include,lib,lib64,libexec,sbin,man,share/doc,share/man}
for i in $(seq 8); do
    mkdir -p $PPI_PREFIX/man/man$i $PPI_PREFIX/share/man/man$i
done
touch $PPI_INSTALLED

export LD_LIBRARY_PATH="${PPI_PREFIX}/lib:$LD_LIBRARY_PATH"

in_tmpdir() {
    tmpdir=$(mktemp -d)
    trap 'rm -rf $tmpdir' EXIT
    cd $tmpdir
}

decompress() {
    # Decompress a file with the given name on stdin
    name=$1
    if [[ $1 == *.bz2 ]]; then
        bzip2 -cd
    elif [[ $1 == *.gz ]]; then
        gzip -cd
    elif [[ $1 == *.tgz ]]; then
        gzip -cd
    elif [[ $1 == *.xz ]]; then
        xz -cd
    else
        echo "Compression type not recognized for: $1"
        exit 1
    fi
}

ppi-pkginfo() {
    pkg="$1"
    pkginfo=${PPI_DESC}/$pkg
    echo "declare -A copy" && cat $pkginfo
}

ppi-src-archive() {
    echo "${PPI_SRCPKG}/${1}/${2}${PPI_SRCEXT}"
}

ppi-bin-archive() {
    echo "${PPI_BINPKG}/${1}/${2}${PPI_BINEXT}"
}

ppi-user-shell() {
    getent passwd $LOGNAME | cut -d: -f7
}

[ -f "$STOW" ] || {
    mkdir -p $PPI_STOW/stow-${STOW_VERSION}
    in_tmpdir
    curl http://ftp.gnu.org/gnu/stow/stow-${STOW_VERSION}.tar.bz2 \
        | tar --strip 1 -xjf -
    cat <<"EOF" |
--- a/lib/Stow.pm.in	2012-02-18 20:33:34.000000000 +0000
+++ b/lib/Stow.pm.in	2014-06-08 22:46:03.420893651 +0100
@@ -1732,7 +1732,7 @@
     }
     elsif (-l $path) {
         debug(4, "  read_a_link($path): real link");
-        return readlink $path
+        (return readlink $path)
             or error("Could not read link: $path");
     }
     internal_error("read_a_link() passed a non link path: $path\n");
EOF
    patch -d . -p 1
    ./configure --prefix=$PPI_STOW/stow-${STOW_VERSION}
    make install
    rm -rf *
    cd -
}
