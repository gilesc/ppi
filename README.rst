============================================
ppi - a personal package installer for Linux
============================================

PPI is a simple package manager that can download, compile, and
install packages on Linux without using root. It attempts to guess
the right way to compile and install different types of packages,
and thus it is very trivial (often a one-liner) to add a new
package.

Why another package manager?
============================

Almost all the major Linux package managers (yum, apt, pacman,
etc.) require root privileges to install packages. In the bad old
days, it made sense to install only one copy of packages, because
disk space was at a premium. Now that this is no longer true, most
programming languages provide a facility to install multiple
versions of packages without root (pip, maven, gem, etc.), but
systems for installing binary packages are lagging behind in this
regard.

There are a few more modern Linux package managers (nix, guix) that
can install without root, but they are complex and difficult to
understand. If you know basic bash syntax, you can use PPI, and you
can very easily write package descriptions, very often just by
specifying the URL to your package's source.

Example use cases:

- Allowing users to easily install packages in an HPC or shared
  hosting environment without providing root
- A simple way to distribute binary dependencies for your programs
  in a platform-independent way
- Bootstrapping chroots, fake chroots, or Linux containers
- Creating a "binary virtualenv" for your applications with
  specific versions of binary dependencies (this is not yet
  implemented).

**PPI is NOT meant to replace your distribution's package
manager.** Instead, it assumes a minimal Linux environment already
exists, and installs "non-core" packages on top of that in your
user's home directory. As an example, I am a scientist, so I
install core Linux utilities using my distro, and science-specific
utilities using PPI.

PPI builds packages from source, but can also make binary packages.
It is heavily inspired by Arch Linux PKGBUILDs and Gentoo's build
system. The main difference from Arch Linux's PKGBUILDs is that it
tries to keep a very minimalistic, declarative description of
packages and deduce on its own how to build the package, rather
than require developers to imperatively specify, step-by-step, how
every package will be built.

Installation
============

Naturally, PPI does not require root to install, but does require
a few dependencies to exist on the platform already, namely:

- curl
- bash v4+ (this may be relaxed in the future)
- GNU gcc/g++, make, and autoconf
- gzip

Most Linux distributions will already have these installed. Then,
simply run:

.. code-block:: bash

    curl https://cdn.rawgit.com/gilesc/ppi/master/bootstrap.sh | bash

This will download and compile a few additional dependencies, then
you will be ready to go!

Basic usage
===========

The program is called ``ppi``, and has several subcommands. The
most common task will be installing packages, which is done like so:

.. code-block:: bash

    ppi install coreutils 8.23

The first argument is the package name, and the second is the
package version. This is optional -- there is a database of 
package versions, but it may not always be up to date or what
you want.

When you install a new version of a package, the old version
remains installed, but the new version is symlinked into your
``PATH`` (and the symlinks from the old version are removed).
This is accomplished using GNU Stow.

The command ``ppi install`` downloads the package, compiles it,
installs it, and symlinks it, all at once. You can also do these
steps individually if you want -- ``ppi fetch <package> <version>``
will download a package, and ``ppi build <package> <version>`` will
compile it.

Selecting among multiple versions
=================================

You may wish to switch between different versions of a program. The
command ``ppi select <package> <version>`` will enable the
specified version (and disable others). ``ppi disable ..`` will
disable a program (remove it from your PATH), but not uninstall it.

Installing multiple packages and virtual environments
=====================================================

It can be difficult to remember the versions of packages you want.
Also, some programs can require very specific versions of
dependencies. You can put multiple packages into a plain text
file with two columns, space-delimited: the first column is the
package name, and the second is the version. Here is an example:

::

    datamash 1.0.6
    parallel latest
    bedtools master
    lz4 r124

You can then install all packages at once using:

.. code-block:: bash

    $ ppi install -f mypackages.txt
    
It is also planned to provide a wrapper script to allow you to
execute programs in a "virtual environment" consisting of the
versions of packages you specify. This is not yet implemented.

Package description basics
==========================

You may want to install a package that PPI does not know how to
find the source for. This file is a simple bash script, and in many
cases, only one thing must be specified: the URL to the package.
You must specify the version as a bash variable. The package
description for the GNU coreutils looks like this:

.. code-block:: bash

    url=http://ftp.gnu.org/gnu/coreutils/coreutils-${version}.tar.xz

That's it! PPI has all it needs to build and install the coreutils.
If your package, instead, is a git repository, the file is very similar:

.. code-block:: bash

    url=git://github.com/arq5x/bedtools.git

Note that here, there is no version variable. That is because the
version will instead be the git branch or commit hash (the latter
not implemented). So, when installing a git-based package, the
"version" will frequently be "master".

When you have created such a file, you simply copy it to your
``$PPI_ROOT/db/desc/`` directory, and make the file name the same
as the package name (By default, the ``$PPI_ROOT`` is
``$HOME/.ppi/``).

More complex package descriptions
=================================

Sometimes compilations need special flags to ``make``,
``configure``, etc, or special environment variables like CFLAGS.
Also, frequently packages depend on other packages. There are
facilities for this.

(To be documented.)

License
=======

GNU GPLv3. No warranty.
