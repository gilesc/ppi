#!/usr/bin/env bash

url=https://bitbucket.com/gilesc/ppi.git

tmpdir=$(mktemp -d)
trap 'rm -rf $tmpdir' EXIT
cd $tmpdir

mkdir -p $PPI_PREFIX/bin
export PATH="$PPI_PREFIX/bin":$PATH
git clone $url .
source libppi.sh

mkdir -p $PPI_ROOT/db/
cp -r versions desc $PPI_ROOT/db/
./ppi install ppi master
