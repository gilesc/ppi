#!/usr/bin/env bash

# Dependencies:
# - tar, make, autotools, gcc
#   - probably bison/flex/yacc for many packages
# - bash v4
# - gzip, bzip2, xz
# - curl

source $(dirname $0)/libppi.sh

ppi-select() {
    pkg="$1"
    version="$2"

    touch installed.new
    cat $PPI_ROOT/db/installed \
        | awk -vpkg=$pkg '
            $1 == pkg { print $1"-"$2 } 
            $1 != pkg { print >> "installed.new" }'
    grep "$pkg " $PPI_INSTALLED \
        | tr ' ' '-' \
        | while read p; do
        echo "deselecting: $p" 
        $STOW -t $PPI_PREFIX -d $PPI_STOW -D $p
    done
    mv installed.new $PPI_ROOT/db/installed
    echo $STOW -t $PPI_PREFIX -d $PPI_STOW -S ${pkg}-${version}
    $STOW -t $PPI_PREFIX -d $PPI_STOW -S ${pkg}-${version}
    echo $pkg $version >> $PPI_ROOT/db/installed
    echo "selected: $pkg-$version" 
}

ppi-deselect() {
    pkg="$1"
    version="$2"
    grep -q "$pkg $version" $PPI_INSTALLED || {
        echo "not installed: $pkg-$version"
        exit 1
    }
    $STOW -t $PPI_PREFIX -d $PPI_STOW -D ${pkg}-${version}
    grep -v "$pkg $version" $PPI_INSTALLED > tmp
    mv tmp $PPI_INSTALLED
    echo "deselected: $pkg-$version"
}

ppi-env() {
    env="$1"
    shift
    if [ -z "$PPI_ENV_SET" ]; then
        export PPI_ENV_SET=y
        p="$PPI_PREFIX"
        export PATH="$p/bin:$PATH"
        export LD_LIBRARY_PATH="$p/lib:$p/lib64:$p/libexec:$LD_LIBRARY_PATH"
        shell=$(ppi-user-shell)
        $shell "$@"
    else
        echo "nested environments not implemented"
        exit 1
    fi
}

cmd="$1"
shift
[ $# -lt 1 ] && {
    cat <<EOF
    No argument.
EOF
    exit 1
}
ppi-$cmd "$@" 1>&2
